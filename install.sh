#!/usr/bin/env bash
cp vimrc ~/.vimrc
if [ $? == 0 ]
then
	echo "Success installing .vimrc"
else
	echo "Failed to install .vimrc"
fi

if [ -d ~/.vim ]
then
    cd dot_vim; cp -r ./* ~/.vim/
else
    cp -r dot_vim ~/.vim
fi

if [ $? == 0 ]
then
	echo "Success install .vim"
else
	echo "Failed to install .vim"
fi
