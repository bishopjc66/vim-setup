set background=dark
set softtabstop=4
set guifont=SimSun\ 18
set background=dark
filetype on
syntax on
filetype plugin indent on
colorscheme distinguished
au FileType python set omnifunc=pythoncomplete#Complete
let g:SuperTabDefaultCompletionType = "context"
let g:pep8_map='<leader>8'
let g:pyflakes_use_quickfix = 0
map <leader>td <Plug>TaskList
map <leader>n :NERDTreeToggle<CR>
